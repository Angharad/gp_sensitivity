#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
This module has all the shared global constants for the emulator work.

Author:
    Angharad Stell, angharadstell@gmail.com
"""
import numpy as np

import acrg_grid.areagrid as areagrid
import acrg_mozart.acrg_MOZART_angharad as mzt

# Useful global constants:
# Years being examined
START_YEAR = 1996
END_YEAR = 2013
YEARS = range(START_YEAR, END_YEAR)
BURN_IN = 4
# Get resolution of original imported files
MZT_FILE = mzt.read("/data/as16992/MOZART/CH4/output/d13_spinup_1999_high/" + 
                    "d13_spinup_high_1/1999/d13_spinup_high_1.mz4.h0.1999-01-01-21600.nc")
LAT_ORIG = MZT_FILE['lat'].values
LON_ORIG = MZT_FILE['lon'].values
LEV = MZT_FILE['lev'].values[-6:]
AREA_ORIG = areagrid(LAT_ORIG, LON_ORIG) * 1.0E4
# Set resolution of desired imported files
LAT = np.arange(-90.0, 102.0, 12.0)
LON = np.arange(0.0, 360.0, 11.25)
AREA = areagrid(LAT, LON) * 1.0E4

#Changing parameters:
#First is delta value of the source in permille, then flux in CH4Tgyr-1.
#Values based on spreadsheet poss_params_3.
TITLES = ['we sd', 'fw sd', 'ag sd', 'ri sd', 'wa sd', 'ff sd', 'bb sd', 'vo sd',
          'we sm', 'fw sm', 'ag sm', 'ri sm', 'wa sm', 'ff sm', 'bb sm', 'vo sm',
          'OH lm', 'ST lm', 'Cl lm', 'so lm',
          'we t1', 'fw t1', 'ag t1', 'ff t1', 'OH t1',
          'qm su', 'qd su', 'ql su']
MIN_VALUES = np.array([-63.3, -64.6, -75.2, -66.0, -57.7, -45.1, -27.9, -46.1,
                       136.0, 54.0, 86.0, 21.0, 46.0, 104.0, 14.0, 27.0,
                       414.0, 6.0, 12.0, -8.0,
                       -0.1, -0.1, -0.1, -0.1, -0.1,
                       495.0, -55.6, 23.2]) 
MAX_VALUES = np.array([-59.7, -59.8, -58.4, -58.2, -53.5, -38.4, -16.5, -41.9,
                       250.0, 198.0, 122.0, 40.0, 69.0, 162.0, 29.0, 62.0,
                       730.0, 55.0, 41.0, -52.0,
                       0.1, 0.1, 0.1, 0.1, 0.1,
                       976.0, -53.7, 62.8])
DEFAULT = (MIN_VALUES + MAX_VALUES) / 2

#Unchanging parameters:
TITLES_UC = ['te sd', 'hy sd', 'oc sd', 'so ld', 
             'ST ld', 'Cl ld', 'te sm', 'hy sm', 'oc sm']
MIN_VALUES_UC = np.array([-66.7, -63.0, -51.7, -24.,
                          0.958, 6.66E-12, 5.0, 0., 8.3])
MAX_VALUES_UC = np.array([-63.3, -61.4, -44.1, -19.,
                          0.966, 6.68E-12, 14.2, 0.9, 23.7])
# Default values for unchanging parameters
DEFAULT_UC = (MIN_VALUES_UC + MAX_VALUES_UC) / 2

# Set hydrate default to zero
DEFAULT_UC[7] = 0.

#plot_output

CASE = 'em_attempt5'
CASE_TEST = 'em_attempt5_test'
CASE_NORM = 'em_attempt5_norm'
CASE_UC = 'em_attempt5_uc'
RUN_PER_FIL = 3
CHUNK = 30
CHUNK_SIZE = CHUNK * RUN_PER_FIL
NO_RUN = 270
NO_RUN_TEST = 90
NO_RUN_NORM = 3
NO_RUN_UC = 90
LEN_OUT = (END_YEAR - START_YEAR) * 12
SHORT_LEN_OUT = (END_YEAR - (START_YEAR + BURN_IN)) * 12
NO_OUT = 4

#gen_in_scalings

# Filename of latin hypercube design
LHC_STR = 'data/run_{:03d}/in_scaling'.format(NO_RUN)
LHC_STR_UC = 'data/in_scaling_uc'
LHC_STR_TEST = 'data/in_scaling_test'
LHC_STR_NORM = 'data/in_scaling_norm'
