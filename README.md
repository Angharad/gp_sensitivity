# README #

This repo contains the scripts used to create the Gaussian process emulators for MOZART, as described in an ACP paper: https://doi.org/10.5194/acp-21-1717-2021. The code to create the emulators is based off the Stilt package 
(https://doi.org/10.32614/RJ-2018-049) and the small changes I made to this are included in this repo.

This repo runs using observational data from Matt Rigby's 2017 paper (https://doi.org/10.1073/pnas.1616426114), there is sometimes an option to not use this data but this will not work in this version of the repo, because the
sensitivity paper does not use my observations. This will change in the next paper / version of this code.

All the data required to run the scripts is included in gp_data (https://osf.io/z435m/) so you should be able to run only parts of the analysis if you want. I would recommend downloading the gp_data file if only to
get the assumed data directory structure that the files will put output into.

There is an option throughout the repo for "site emulators" which emulate a single grid cell in MOZART. The data required for this has not been included as it is a work in progress, but watch out for 
my next paper/ version of this code, which will include this option. So for the scripts below to work you have to set the sites variable to FALSE.


The repo is set out in subdirectories. To run a script in a subdirectory, you'll need to have the subdirectory as the working directory (cd into it in the command line or set as working directory
in Rstudio). 

Optimising the emulators (runnning training/train_emulators.R) relies on you having the right version of R. I've found that both 3.2 and 4.0 optimise the emulators correctly, but 3.6 does not.

The list below lays out a logical order to run the scripts in, a summary of what the script does, and how to run the script from the command line. If you would like to run the scripts looking more in 
depth at the code (e.g. in Rstudio), then just fill in the commandArgs inputs at the top of each script.
  
### How to run the scripts ###

* training
    * train_emulator.R
	    * this script trains the emulators and chooses the optimal emulator out of however many cores you have
	    * run command example: Rscript train_emulator.R 1 1 270 FALSE ~
		* inputs: 
		    1. Pick observation type: 1 for mole fraction, 2 for delta value
			2. Pick emulator location: 1 for southern hemisphere, 2 for northern hemisphere (or site number for sites)
			3. Pick number of runs used to train emulator: 270 simulations in training dataset, other options are 90, 60
			4. Pick whether to emulate hemispheres or sites: FALSE for hemispheres, TRUE for sites
			5. Where to get/ put output: path to where you have downloaded gp_data
		* outputs:
		    1. optimisation log in gp_data/emulators/optimisation_logs
			2. optimised emulator in gp_data/emulators
* validation
    * validation_graphs.R
	    * this script plots a lot of graphs to test if the emulator is doing a good job
		* run command example: Rscript validation_graphs.R 270 FALSE TRUE ~ ~/gp_plots/validation
		* inputs: 
		    1. Pick number of runs used to train emulator: 270 simulations in training dataset, other options are 90, 60
			2. Pick whether to emulate hemispheres or sites: FALSE for hemispheres, TRUE for sites
			3. Pick whether to use test data or LOOCV: FALSE for LOOCV, TRUE for test data
			5. Where to get/ put output: path to where you have downloaded gp_data
			6. Where to put plots: path to where you have downloaded gp_data
		* outputs:
		    1. list of emulators to easily read into future scripts in gp_data/emulators
			2. a lot of validation plots into your plot directory
			3. if LOOCV, RMSE saved in /gp_data/emulators/em_rmse for comparison of different training datasets in how_many_runs_needed.R
			4. list of combined useful errors and observations saved in gp_data/combined to be used in future scripts
* em_info
    * comp_mzt_obs.R
	    * this script compares the MOZART training dataset to the observations
	    * run command example: Rscript comp_mzt_obs.R ~ ~/gp_plots/em_info
		* inputs:
			1. Where to get/ put output: path to where you have downloaded gp_data
			2. Where to put plots: path to where you have downloaded gp_data
		* outputs:
		    1. Plots comparing MOZART training runs and observations into plot directory
		
    * how_good_is_mlr.R
	    * this script compares the performance of multiple linear regression and the gaussian process in emulating MOZART
	    * run command example: Rscript how_good_is_mlr.R ~ ~/gp_plots/em_info
		* inputs:
			1. Where to get/ put output: path to where you have downloaded gp_data
			2. Where to put plots: path to where you have downloaded gp_data
		* outputs:
		    1. Plots comparing MLR and Gaussian process's ability to emulate MOZART, saved into plot directory
			
    * how_many_runs_needed.R
	    * this script compares the performance of Gaussian processes trained with different numbers of MOZART simulations
	    * run command example: Rscript how_many_runs_needed.R ~ ~/gp_plots/em_info
		* inputs:
			1. Where to get/ put output: path to where you have downloaded gp_data
			2. Where to put plots: path to where you have downloaded gp_data
		* outputs:
		    1. Plots comparing the ability of the emulators trained with different numbers of runs to emulate MOZART, saved into plot directory
	
* sensitivity
    * more_complex_sensitivity_chap5_calc.R
	    * this script calculates the sensitivity indices (probably better off not running this on your laptop with large numbers)
		* run command example: Rscript more_complex_sensitivity_chap5_calc.R 1 1000 1000 ~
		* inputs:
		    1. A number from 1:30 to calculate a different part of the sensitivity: 1 to calculate A, 2 to calculate B, 3 for 1st emulator sensitivity, 4 for 2nd emulator sensitivity...
			2. Size of arrays of emulator predictions to determine sensitivity (30000 used in paper), this will be multiplied by the number of cores on your machine (60000 means 840000 is the number 
			   of runs in the A/B/C matrices if you have 28 cores like me)
			3. Number of bootstrap resamples to get uncertainty (10000 used in the paper)
			4. Where to get/ put output: path to where you have downloaded gp_data
		* outputs:
		    1. part of the sensitivity calculation, saved into gp_data/sensitivity
		
	* more_complex_sensitivity_chap5_plots.R
	    * this script plots the sensitivity indices
		* run command example: Rscript more_complex_sensitivity_chap5_plots.R ~ ~/gp_plots/sensitivity
		* inputs:
			1. Where to get/ put output: path to where you have downloaded gp_data
			2. Where to put plots: path to where you have downloaded gp_data
		* outputs:
		    1. Plots showing the sensitivity indices for the different outputs, saved into the plot directory
			
	* delta_value_and_oh.R
	    * this script plots OAT sensitivity tests, used to think about delta value and OH
		* run command example: Rscript delta_value_and_oh.R ~ ~/gp_plots/sensitivity
		* inputs:
			1. Where to get/ put output: path to where you have downloaded gp_data
			2. Where to put plots: path to where you have downloaded gp_data
		* outputs:
		    1. Plots showing how the delta value changes by perturbing one parameter at a time, saved into the plot directory
  

### Who do I talk to? ###

If you do run into problems, contact us at Matt.Rigby@bristol.ac.uk.
