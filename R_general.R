# Written by Angharad Stell
# Widely used R functions


# Constants ---------------------------------------------------------------


# Colourblind palette
cbbPalette <- c("#000000", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")

# Plot fontsizes
grid.fontsize <- 16#14
legend.fontsize <- 16#14
plot.fontsize <- 14#12
single.plot.fontsize <- 16#14

# Graph labels
measure.type <- rep(c("CH[4]", "delta^{13} ~ C-CH[4]"), times=3)
mean.type <- rep(c("global mean", "inter-hemispheric difference", "trend"), each=2)
subplot.labels <- sapply(1:6, function(x) sprintf("paste(%s, ' %s')", measure.type[x], mean.type[x]))

delta.parse <- "~delta^{13} ~ C-CH[4]"
full.names <- c(paste0("Wetlands~source", delta.parse), paste0("Freshwater~source", delta.parse), paste0("Agricultural~source", delta.parse),
                paste0("Rice~source", delta.parse), paste0("Waste~source", delta.parse), paste0("Fossil~fuels~source", delta.parse),
                paste0("Biomass~burning~source", delta.parse), paste0("Volcanic~source", delta.parse),
                "Wetlands~source~magnitude", "Freshwater~source~magnitude", "Agricultural~source~magnitude", "Rice~source~magnitude",
                "Waste~source~magnitude", "Fossil~fuels~source~magnitude", "Biomass~burning~source~magnitude", "Volcanic~source~magnitude",
                "OH~loss~magnitude", "Stratospheric~loss~magnitude", "Cl~loss~magnitude", "Soil~loss~magnitude",
                "Wetlands~trend", "Freshwater~trend", "Agricultural~trend", "Fossil~fuels~trend", "OH~trend",
                "Spin-up~source~magnitude", paste0("Spin-up~source", delta.parse), "Spin-up~loss~magnitude")


# Functions ---------------------------------------------------------------


#' Like list comprehension
#' @param l A list to comprehend
#' @param param A string of the parameter name to read in
#' @return A vector of the desired parameter had been in a list
list.comp <- function(l, param="lik") {
  vec <- vector(mode="numeric", length=length(l))
  for (i in 1:length(l)) {
    vec[i] <-l[[i]][param]
  }
  print(vec)
  #!+
  vec
}

#' Get the filename suffix to read/ save objects
#' @param uc A boolean: TRUE for use uc data
#' @param test A boolean: TRUE for use test data
#' @return The string of the filename suffix
get_filename <- function(uc=FALSE, test=FALSE){
  if (uc) {
    filename <- '_uc'
  } else {
    if (test) {
      filename <- '_test'
    } else {
      filename <- ''
    }
  }
  filename
}

# Get MOZART site locations
#' @param data.dir A string of the path where data directory gp_data
#' @param measure A string of "mf" for mole fraction or "de" for delta value
#' @return A data.frame of site names and locations
get.site.loc <- function(data.dir, measure) {
  site.loc <- read.csv(sprintf("%s/gp_data/obs/mzt_site_indices_%s.csv", data.dir, measure), row.names=1)
  # Change from python to R index
  site.loc <- site.loc + 1
  
  site.loc
}

#' Read in Matt's observations observations
#' @param data.dir A string of the path where data directory gp_data
#' @return A list of the observations and its errors
get.matts.obs <- function(data.dir) {
  matts.obs.mf <- read.csv(sprintf("%s/gp_data/obs/matts_obs/data_hemispheric_NOAA_CH4.csv", data.dir))
  matts.obs.de <- read.csv(sprintf("%s/gp_data/obs/matts_obs/data_hemispheric_INSTAAR_dCH4.csv", data.dir), skip=1)
  mask <- matts.obs.mf["Time"] > 2000. & matts.obs.mf["Time"] < 2013.
  
  matts.obs <- list(matts.obs.mf[mask, "SH"], matts.obs.mf[mask, "NH"], matts.obs.de[mask, "SH"], matts.obs.de[mask, "NH"])
  matts.obs.std <- list(matts.obs.mf[mask, "SH_variability"], matts.obs.mf[mask, "NH_variability"], 
                        matts.obs.de[mask, "SH_variability"], matts.obs.de[mask, "NH_variability"])
  
  obs.list <- list(obs=matts.obs, obs.std=matts.obs.std)
}

#' Get numeric values from the python config file
#' @param keyword A string to extract the numeric value for from the python config file
#' @return The numeric value of the parameter in the config file
awk <- function(keyword) {
  # Python configuration file
  cfg <- "../MZT_runs/cfg.py"            
  # Get awk string to search for
  search.string <- sprintf("'/%s = /{ print $3 }'", keyword)    
  
  # Return result of awk command
  as.numeric(system(paste("awk ", search.string, cfg), intern=TRUE))
}

# Read in cfg constants fro other scripts
START.YEAR <- awk("START_YEAR") 
END.YEAR <- awk("END_YEAR") 
BURN.IN <- awk("BURN_IN") 
SHORT.LEN.OUT <- (END.YEAR - (START.YEAR + BURN.IN)) * 12   


# Simple functions to improve ggplots -------------------------------------


#' Remove the default grid and border around the plot
#' @param p A ggplot
#' @return An improved ggplot
blank.background <- function(p) {
  p + theme(panel.grid=element_blank(), panel.border=element_blank())
}

#' Put the legend at the top and remove the legend title
#' @param p A ggplot
#' @return An improved ggplot
top.legend <- function(p) {
  p + theme(legend.position="top", legend.title=element_blank())
}

#' Remove the x and y axis titles
#' @param p A ggplot
#' @return An improved ggplot
blank.xy.titles <- function(p) {
  p + theme(axis.title.x=element_blank(), axis.title.y=element_blank())
}

#' Add a central title to the plot
#' @param p A ggplot
#' @param title.str A string, the title of the plot
#' @return An improved ggplot
centre.title <- function(p, title.str) {
  p + labs(title=title.str) + theme(plot.title=element_text(hjust=0.5))
}

#' Cut down the x range of a plot
#' @param p A ggplot
#' @param xlim A numeric vector, the minimum and maximum x values
#' @return An improved ggplot
scale.x <- function(p, xlim) {
  p + scale_x_continuous(limits=xlim, expand=c(0, 0))
}

#' Cut down the x range of a plot and label in years
#' @param p A ggplot
#' @return An improved ggplot
scale.time.x <- function(p) {
  p + scale_x_continuous(limits=c(1, SHORT.LEN.OUT), labels=seq(START.YEAR+BURN.IN, END.YEAR, 3), 
                         breaks=seq(1, SHORT.LEN.OUT, 36), expand=c(0, 0))
}

#' Cut down the x range of a plot and label from 0 to 1
#' @param p A ggplot
#' @return An improved ggplot
scale.param.x <- function(p) {
  p + scale_x_continuous(limits=c(0, 1), labels=seq(0, 1, 0.5), breaks=seq(0, 1, 0.5), expand=c(0, 0))
}

#' Cut down the y range of a plot
#' @param p A ggplot
#' @param ylim A numeric vector, the minimum and maximum y values
#' @return An improved ggplot
scale.y <- function(p, ylim) {
  p + scale_y_continuous(limits=ylim, expand=c(0, 0))
}

#' Save a ggplot as a pdf
#' @param plot.function A function that returns the plot to be saved
#' @param filename A string to save the file as
#' @param width A numeric value of the width of the plot
#' @param height A numeric value of the height of the plot
save.plot.cairo <- function(plot.function, filename, width=10, height=10) {
  cairo_pdf(filename, family="DejaVu Sans", height=height, width=width)
  plot.function()
  dev.off()
}

#' Create a function that pads ggplot labels in the legend
#' @param npad A numeric value of how much to pad
#' @return A function that pads ggplot labels in the legend by npad
str_pad_custom_npad <- function(npad){
  function(labels){
    new_labels <- stringr::str_pad(labels, npad, "right")
    return(new_labels)
  }
}

#' Annotate subplots with a letter and label in the top right corner
#' @param p A ggplot
#' @param index A numeric value (will be converted to a letter) to label the ggplot
#' @param label A string to label the ggplot
#' @param s A numeric value for the size of the annotation
#' @return A labeled ggplot
plot.annotate <- function(p, index, label, s=5) {
  alphabet.letters <- 26
  if (index > alphabet.letters && index < (2*alphabet.letters)) {
    indexed.label <- sprintf("paste('(a%s) ', %s)", letters[index %% alphabet.letters], label)
  } else if (index <= alphabet.letters) {
    indexed.label <- sprintf("paste('(%s) ', %s)", letters[index], label)
  } else {
    print("Too many plots")
  }
  p  + annotate(geom="text", label = indexed.label, x=-Inf, y=Inf, hjust=0, vjust=1, size=s, parse=T)
}

# Make y axis sscale 10 % larger
yscale.ten.percent <- function(p, errbar=FALSE, lowerbound=NA, fraction=0.1) {
  if (errbar) {
    yvalues <- c(ggplot_build(p)$data[[1]]$y, ggplot_build(p)$data[[2]]$ymin, ggplot_build(p)$data[[2]]$ymax)
  } else {
    yvalues <- ggplot_build(p)$data[[1]]$y
  }
  scale.y(p, c(lowerbound, max(yvalues, na.rm=TRUE) + abs(diff(range(yvalues, na.rm=TRUE)))*fraction))
}

# Create layout matrix for sites grid plot that separates mf and de
measure.matrix <- function(no.sites, plots.col) {
  first.section <- t(sapply(seq(1, (no.sites - no.sites %% plots.col), plots.col), function(x) x:(x+plots.col-1)))
  if (no.sites %% plots.col != 0) {
    fill.na <- rep(NA, (plots.col - no.sites %% plots.col))
    second.section <- c((no.sites - no.sites %% plots.col + 1):no.sites, fill.na)
  } else {
    second.section <- NULL
  }
  rbind(first.section, second.section)
}

# Create layout matrix for grid plot
layout.labelled.matrix.build <- function(no.mf.sites, no.de.sites, sites) {
  no.sites <- no.mf.sites + no.de.sites
  
  if (sites) {
    plots.col <- 3 
  } else {
    plots.col <- 2 
  }
  
  layout.col <- plots.col + 1  
  
  labels.col <- c(rep(no.sites+1, ceiling(no.mf.sites / plots.col)), rep(no.sites+2, ceiling(no.de.sites / plots.col)))
  mf.matrix <- measure.matrix(no.mf.sites, plots.col)
  de.matrix <- measure.matrix(no.de.sites, plots.col) + no.mf.sites
  
  layout.labelled.matrix <- cbind(labels.col, rbind(mf.matrix, de.matrix))
  colnames(layout.labelled.matrix) <- NULL
  layout.labelled.matrix <- rbind(rep(1, layout.col), layout.labelled.matrix+1)
  
  layout.widths <- c(0.3, rep(3, (layout.col-1)))
  layout.heights <- c(0.3, rep(1, (dim(layout.labelled.matrix)[1]-1)))
  
  list(matrix=layout.labelled.matrix, widths=layout.widths, heights=layout.heights)
}

# Create layout matrix for sites grid plot for only mf or de
layout.labelled.matrix.sep.build <- function(no.sites, no.col) {
  matrix <- measure.matrix(no.sites, no.col) + 1
  ylabel <- rep(no.sites+2, ceiling(no.sites / no.col))
  legend <- rep(1, no.col+1)
  
  layout.matrix <- rbind(legend, cbind(ylabel, matrix))
  layout.heights <- c(0.3, rep(1, dim(matrix)[1]))
  layout.widths <- c(0.3, rep(3, no.col))
  
  list(matrix=layout.matrix, widths=layout.widths, heights=layout.heights)
}

# Basic combinations of simple ggplot functions ---------------------------


#' Basic combination to improve plot for grid, arrange plotting: 
#' make the plot background blank, put in a central title,
#' and remove the x and y axis labels
#' @param p A ggplot
#' @param title.str A string, the title of the plot
#' @return An improved ggplot
grid.base.title <- function(p, title.str) {
  p <- blank.background(p)
  p <- centre.title(p, title.str)
  p <- blank.xy.titles(p)
  
  p
}
#' Basic combination to improve plot for grid, arrange plotting: 
#' make the plot background blank, put in a central title,
#' and remove the x and y axis labels
#' @param p A ggplot
#' @param index A numeric value (will be converted to a letter) to label the ggplot
#' @param label A string to label the ggplot
#' @param s A numeric value for the size of the annotation
grid.base.annotate <- function(p, index, title.str, s=5) {
  p <- blank.background(p)
  p <- plot.annotate(p, index, title.str, s)
  p <- blank.xy.titles(p)
  
  p
}

#' Rescale both the x and y axes
#' @param p A ggplot
#' @param xlim A numeric vector, the minimum and maximum x values
#' @param ylim A numeric vector, the minimum and maximum y values
#' @return An improved ggplot
scale.xy <- function(p, xlim, ylim) {
  p <- scale.x(p, xlim)
  p <- scale.y(p, ylim)
  
  p
}

# Remove x axis labels
remove.x.labels <- function(p) {
  p <- p + theme(axis.text.x=element_blank(), axis.ticks.x=element_blank())
  
  p
}

# Remove y axis labels
remove.y.labels <- function(p) {
  p <- p + theme(axis.text.y=element_blank(), axis.ticks.y=element_blank())
  
  p
}

# Functions to create shared legend in grid plot --------------------------


#' Get the legend from a plot
#' @param myggplot A ggplot
#' @return The legend of the plot
get_legend <- function(myggplot){
  tmp <- ggplot_gtable(ggplot_build(myggplot))
  leg <- which(sapply(tmp$grobs, function(x) x$name) == "guide-box")
  legend <- tmp$grobs[[leg]]
  
  legend
}

#' Get the legend from a list of plots, then remove the individual legends
#' @param plot.list A list of ggplots
#' @return The list of ggplots with no legends and a separate legend as part of the list
extract.legend <- function(plot.list) {
  plot.list[[1]] <- get_legend(plot.list[[2]])
  plot.list[2:length(plot.list)] <- lapply(1:(length(plot.list) - 1), function(x) plot.list[[(x+1)]] + theme(legend.position="none"))
  
  plot.list
}


#' Function to extract maximum and minimum parameter values
#' @param emul An emulator object
#' @return A list containing the minimum and maximum parameter values
emul.in.range <- function(emul) {
  par.min             <- apply(emul$Theta.mat, 2, min)
  par.max             <- apply(emul$Theta.mat, 2, max)
  
  list(min.params=par.min, max.params=par.max)
}

#' Function to check if the parameter values are in the acceptable range
#' @param theta.star A vector of parameter values to be predicted at
#' @param colMin A vector of parameter minimum values 
#' @param colMax A vector of parameter maximum values 
#' @return A boolean: TRUE for within the range of the emulator, FALSE for outside
check.range <- function(theta.star, colMin, colMax) {
  anyhigh             <- any(theta.star > colMax)
  anylow              <- any(theta.star < colMin)
  if (anyhigh || anylow ) FALSE else TRUE
}

#' Rescale points so all in emulator predictable range
#' @param test.data The matrix of input parameters to predict at
#' @param colMin The minimum acceptable parameter values for the emulator
#' @param colMax The maximum acceptable parameter values for the emulator
#' @return A matrix of the rescaled inputs
data.rescale <- function(test.data, colMin, colMax) {
  test.data.rescale <- sapply(1:dim(test.data)[1], function(x) test.data[x, ] * (colMax - colMin) + colMin)
  
  within.range <- sapply(1:dim(test.data.rescale)[2], function(x) check.range(test.data.rescale[, x], colMin, colMax))
  
  if (!all(within.range)) {
    warning("Some of the runs generated are outside of the emulators' predictable range")
  }
  
  test.data.rescale
}


# Functions to read in data --------------------------

# Read in MOZART hemi netcdf file
read.hemi.nc <- function(ncin, measure, lat.no) {
  data <- ncvar_get(ncin, measure)
  out <- data[lat.no, , ]
  rownames(out) <- seq(1, SHORT.LEN.OUT)
  
  out
}

#' Extract site MOZART runs from netcdf
#' @param measure A string of the desired variable name
#' @param ncin The netcdf object
#' @param lat.no The numeric value of the desired site number to extract
#' @return A matrix of the MOZART output
read.sites.nc <- function(measure, ncin, lat.no) {
  mzt.raw <- ncvar_get(ncin, measure)
  
  # Read in site locations
  loc <- get.site.loc(data.dir, measure)
  
  out <- t(mzt.raw[loc$lon[lat.no], loc$lat[lat.no], loc$alt[lat.no], , ])
  
  # Make look like processed outputs
  rownames(out) <- seq(1, SHORT.LEN.OUT)
  
  out
}

# Extract hemi data from MOZART hemi netcdf
out.extract <- function(ncin, sites) {
  no.mf.sites <- ncvar_get(ncin, "no_mf_sites")
  no.de.sites <- ncvar_get(ncin, "no_de_sites")
  
  if (sites) {
    outputs.frame.mf <- lapply(1:no.mf.sites, function(x) read.sites.nc("mf", ncin, x))
    outputs.frame.de <- lapply(1:no.de.sites, function(x) read.sites.nc("de", ncin, x))
  } else {
    outputs.frame.mf <- lapply(1:no.mf.sites, function(x) read.hemi.nc(ncin, "mf", x))
    outputs.frame.de <- lapply(1:no.de.sites, function(x) read.hemi.nc(ncin, "de", x))
  }
  
  c(outputs.frame.mf, outputs.frame.de)
}

# Turn NH and SH data into a mean, interhemispheric difference, and trend
box.model.info <- function(sh, nh) {
  box.mean <- (sh + nh) / 2
  box.ihd <- nh - sh
  
  start.index <- 12
  # obs and mzt have different shapes
  if (length(dim(nh)) == 2) {
    box.trend <- box.mean[SHORT.LEN.OUT, ] - box.mean[start.index, ]
  } else {
    box.trend <- box.mean[SHORT.LEN.OUT] - box.mean[start.index]
  }
  
  list(mean=box.mean, ihd=box.ihd, trend=box.trend)
}
